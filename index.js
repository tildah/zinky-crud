const Zinko = require('zinko');
const ObjectID = require("mongodb").ObjectID;

class ZinkyCRUD extends Zinko {

  get useZongel() { return true; }

  get updateAdapt() {return false;}

  async POST_root(req) {
    return await this.model.insertOne(req.body);
  }

  getListQuery(req) { return [{ $match: { trash: { $ne: true } } }]; }

  async $GET_root(req) {
    return await this.model.aggregate(this.getListQuery(req));
  }

  async GET_root(req, res, _id) {
    const filter = { _id: new ObjectID(_id), trash: { $ne: true } };
    return await this.model.findOne(filter);
  }

  async PUT_root(req, res) {
    const filter = { _id: new ObjectID(req.params[0]) };
    const set = { $set: req.body };
    const options = { returnOriginal: false, adapt: this.updateAdapt };
    const ops = await this.model.findOneAndUpdate(filter, set, options);
    if (!ops.value) throw { statusCode: 400, msg: "Item not Found" };
    return ops.value;
  }

  async PATCH_root(req, res, prop, _id, value) {
    const filter = { _id: new ObjectID(_id) };
    const set = { $set: { 
      [prop]: value !== undefined ? value : req.body.value 
    } };
    const options = {adapt: this.updateAdapt};
    const ops = await this.model.findOneAndUpdate(filter, set, options);
    if (!ops.value) throw { statusCode: 400, msg: "Item not Found" };
    return ops.value;
  }

  async DELETE_root(req, res) {
    const filter = { _id: new ObjectID(req.params[0]) };
    const set = { $set: { trash: true } };
    const ops = await this.model.updateOne(filter, set);
    if (!ops.matchedCount) throw { statusCode: 400, msg: "Item not Found" };
    return { statusCode: 204 };
  }

}

module.exports = ZinkyCRUD;
